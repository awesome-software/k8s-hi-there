FROM node:alpine

RUN apk update && \
    apk add --no-cache make gettext

COPY . .

RUN npm install

EXPOSE 3000

CMD [ "npm", "start"]
