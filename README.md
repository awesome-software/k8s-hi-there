# Hi There App

This is a simple app that displays a hello message with the pod name where it is deployed.

## General Hi-There App Functionality

This is an `express` node.js app that with 2 rest end points resources on root ("/") and hi ("/hi").

While the resource on root only displays a simple message `This is the Hi There App!`, the 'hi' 
resource requires an environment variable set using the `dotenv` library and `.env` file. The 'hi'
resource returns `Hi There From: $POD_NAME!` where `POD_NAME` can be found on the `.env` file, 
default is `no-pod` for local runs.

The application can be run locally if node is installed by running:

`npm install` then `npm start`

## Dockerfile

In this project you can also find a Dockerfile that contains the `Hi-There` app and an additional 
package `gettext` to run `envsubst` which is explained below.

The docker image installs all node dependencies with `npm install`, then exposes nodejs default port `3000` and leaves a on going node server running with `npm start`.

This docker image can be found on my DockerHub or on this Gitlab's docker registry:

* DockerHub: `docker pull juanmacoo/k8s-hi-there:latest`
* Gitlab Registry: `docker pull registry.gitlab.com/awesome-software/k8s-hi-there:latest`

Note that to download from gitlab registry you must have valid gitlab credentials to perform:
`docker login registry.gitlab.com`

## Kubernetes Deployment

To deploy the Docker container to a Kubernetes cluster you must have `kubectl` installed. Once it is
installed you can deploy manifests in the following order:

1. ConfigMap: `kubectl apply [-n namespace] -f manifest/configmap.yaml`
2. Deployment: `kubectl apply [-n namespace] -f manifest/deployment.yaml`
3. Service: `kubectl apply [-n namespace] -f manifest/service.yaml`

**Note:** Notice that -n namespace is optional. You may create these objects on a namespace if you
have one.

### Access App

The deployed service is using a ClusterIP. To access it you can use a proxy to access the api server:

` kubectl proxy --port=9090`

This will leave an ongoing process that you must leave as is. Access the hi-there app by following 
the [api server proxy URL](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-services/#manually-constructing-apiserver-proxy-urls):

`<kubernetes_master_address>/api/v1/namespaces/<namespace_name>/services/<service_name>[:port_name]/proxy`

Where:

* **kubernetes_master_address:** is the proxy address or kube-master address (e.g. localhost:9090).
* **namespace_name:** the namespaces where you deployed the k8s objects above (e.g. default, if no
  namespace was defined).
* **service_name:** the name of the service defined in the service.yaml manifest (e.g hi-there-service)
* **port_name:** The port_name of the port you wish to access (e.g. http).

Example:

`http://localhost:9090/api/v1/namespaces/default/services/hi-there-service:http/proxy/hi`

### Alternative Access

If you have a local cluster that can provide you with an IP (e.g. minikube), you know the IP's of 
your cluster nodes or you are working on a cloud provider you may change the service type to NodePort 
or LoadBalancer respectively. Each will generate a different url to access the cluster.

#### NodePort Access

Change the service.yaml `spec.type` to `NodePort` and deploy de service:

`kubectl apply [-n namespace] -f manifest/service.yaml`

This will generate a specific port to access your app. You can find this port out running the following 
kubectl command:

`export NODE_PORT=$(kubectl get [--namespace <namespace>] -o jsonpath="{.spec.ports[0].nodePort}" services hi-there-service)`

**Note:** Notice that -n namespace is optional. You may create these objects on a namespace if you
have one.

Use your IP in conjunction with the node port you just found:

`http://<your_ip>:$NODE_PORT`

If you are using minikube you can find the whole IP for the `NodePort` as follows:

`minikube service [-n namespace] --url hi-there-service`

**Note:** Notice that -n namespace is optional. You may create these objects on a namespace if you
have one.

#### LoadBalancer Access

It may take a few minutes for the LoadBalancer IP to be available. You can watch the status of by 
running:

`kubectl get svc -w hi-there-service`

Once it is finished you can grab the IP from the load balancer by running:

`export SERVICE_IP=$(kubectl get svc [--namespace namespace] hi-there-service -o jsonpath='{.status.loadBalancer.ingress[0].ip}')`

**Note:** Notice that -n namespace is optional. You may create these objects on a namespace if you
have one.

Access your hi-there app running:

`http://$SERVICE_IP:80`

## Optional deployment

If you are using helm you can also deploy a chart with the deployment, configmap, and service.

### Installing Helm

Helm is the Kubernetes package manager used to package, compose, publish, and install Kubernetes applications. You need to install the same version we are running in the cluster.

Find out how to install Helm [here](https://helm.sh/docs/using_helm/#installing-helm).

### Install helm in the cluster

Helm (also known as tiller) must be installed inside the cluster to work. To do so run:

`helm init`

Wait for tiller to be installed. You can check the progress by running:

`kubectl get pod -n kube-system -l"app=helm"`

and wait for all pods to be ready.

### Install the hi-there using helm

You can install the hi-there helm chart by running:

`helm upgrade -i [--namespace <namespace>] hi-there charts/deploy`

**Note:** Notice that -n namespace is optional. You may create these objects on a namespace if you
have one.

#### Access App

The deployed service is using a ClusterIp. To access it you can use a proxy to access the api server:

` kubectl proxy --port=9090`

This will leave an ongoing process that you must leave as is. Access the hi-there app by following 
the [api server proxy URL](https://kubernetes.io/docs/tasks/administer-cluster/access-cluster-services/#manually-constructing-apiserver-proxy-urls):

`<kubernetes_master_address>/api/v1/namespaces/<namespace_name>/services/<service_name>[:port_name]/proxy`

Where:

* **kubernetes_master_address:** is the proxy address or kube-master address (e.g. localhost:9090).
* **namespace_name:** the namespaces where you deployed the k8s objects above (e.g. default, if no
  namespace was defined).
* **service_name:** the name of the service defined in the service.yaml manifest (e.g hi-there-deploy)
* **port_name:** The port_name of the port you wish to access (e.g. http).

Example:

`http://localhost:9090/api/v1/namespaces/default/services/hi-there-deploy:http/proxy/hi`
