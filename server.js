// Imports
const express = require("express");
const dotenv = require('dotenv');

// Globals
const app = express();
const port = 3000;

// Config dotenv
dotenv.config();

// Install json parser
app.use(express.json());
app.use(express.urlencoded({
    extended: true
  }));

app.listen(port, function(){
    console.log("Hi There App");
    console.log("");
});

app.get("/", function(req, res) {
    res.status(200).send("<h1>This is the Hi There App!</h1>");
});

const pod = process.env.POD;
app.get("/hi", function(req, res) {
    res.status(200).send("<h1>Hi There From: " + pod + "!</h1>");
});